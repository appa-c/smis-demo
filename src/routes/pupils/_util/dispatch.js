import { eventStore } from "../../_util/stores";

// Pupils
export async function updatePupil(pupil, upn) {
  request("PUT", `api/pupils/${upn}`, {
    details: {
      message: "updating pupil",
      subject: upn,
      data: pupil,
    },
    body: JSON.stringify(pupil),
  });
}

// SEN Needs //

// Create SEN Need
export function createSenNeed(senNeed, upn) {
  request("POST", `api/pupils/${upn}/sen/needs`, {
    details: {
      message: "creating sen need",
      subject: upn,
      data: senNeed,
    },
    body: JSON.stringify({ ...senNeed }),
  });
}

// Update SEN Need
export function updateSenNeed(senNeed, upn) {
  request("PUT", `api/pupils/${upn}/sen/needs`, {
    details: {
      message: "updating sen need",
      subject: upn,
      data: senNeed,
    },
    body: JSON.stringify(senNeed),
  });
}

// Delete SEN Need
export function deleteSenNeed(senNeed, upn) {
  request("DELETE", `api/pupils/${upn}/sen/needs`, {
    details: {
      message: "deleting sen need",
      subject: upn,
      data: senNeed,
    },
    body: JSON.stringify(senNeed),
  });
}

// FSM Entries //

export function createFsmEntry(fsmEntry, upn) {
  request("POST", `api/pupils/${upn}/fsm/entries`, {
    details: {
      message: "creating fsm entry",
      subject: upn,
      data: fsmEntry,
    },
    body: JSON.stringify({ ...fsmEntry }),
  });
}

export function updateFsmEntry(fsmEntry, upn) {
  request("PUT", `api/pupils/${upn}/fsm/entries`, {
    details: {
      message: "updating fsm entry",
      subject: upn,
      data: fsmEntry,
    },
    body: JSON.stringify(fsmEntry),
  });
}

export function deleteFsmEntry(fsmEntry, upn) {
  request("DELETE", `api/pupils/${upn}/fsm/entries`, {
    details: {
      message: "deleting fsm entry",
      subject: upn,
      data: fsmEntry,
    },
    body: JSON.stringify(fsmEntry),
  });
}

// Contacts
export function createContact(contact, upn) {
  request("POST", `api/pupils/${upn}/contacts`, {
    details: {
      message: "creating contact",
      subject: upn,
      data: contact,
    },
    body: JSON.stringify(contact),
  });
}

export function updateContact(contact, upn) {
  request("PUT", `api/pupils/${upn}/contacts`, {
    details: {
      message: "updating contact",
      subject: upn,
      data: contact,
    },
    body: JSON.stringify(contact),
  });
}

export function deleteContact(contact, upn) {
  request("DELETE", `api/pupils/${upn}/contacts`, {
    details: {
      message: "deleting contact",
      subject: upn,
      data: contact,
    },
    body: JSON.stringify(contact),
  });
}

async function request(method, url, { body, details }) {
  console.log(method, url, body, details);
  let res;
  try {
    res = await fetch(url, {
      method,
      headers: { "Content-Type": "application/json" },
      body,
    });
  } catch (err) {
    throw new Error(err);
  }

  const eventResponse = await res.json();
  eventStore.addSent({
    EventId: eventResponse.data.Entries[0].EventId,
    EventDetail: details,
  });
}

import axios from "axios";
import { eventBridge } from "../../../_aws";

// Get all SEN Needs for a pupil
export async function get(req, res, next) {
  const { upn } = req.params;

  const url = `${process.env.AWS_API_GATEWAY_BASEURL}/pupils/${upn}/senneeds`;
  const data = await axios({
    url,
    method: "GET",
    headers: { "x-api-key": process.env.AWS_API_GATEWAY_KEY },
  });

  if (data !== null) {
    res.end(JSON.stringify(data.data));
  } else {
    next();
  }
}

// Create SEN Need
export async function post(req, res, next) {
  let data;
  const senNeed = req.body;
  try {
    data = await eventBridge
      .putEvents({
        Entries: [
          {
            Source: "naska.ui",
            DetailType: "POST::SEN_NEED",
            Detail: JSON.stringify(senNeed),
            EventBusName: "default",
            Time: new Date(),
          },
        ],
      })
      .promise();
  } catch (err) {
    res.end(JSON.stringify({ err }));
  }
  res.end(JSON.stringify({ data }));
}

// Update SEN Need
export async function put(req, res, next) {
  let data;
  try {
    data = await eventBridge
      .putEvents({
        Entries: [
          {
            Source: "naska.ui",
            DetailType: "PUT::SEN_NEED",
            Detail: JSON.stringify(req.body),
            EventBusName: "default",
            Time: new Date(),
          },
        ],
      })
      .promise();
  } catch (err) {
    res.end(JSON.stringify({ err }));
  }
  res.end(JSON.stringify({ data }));
}

// Delete SEN Need
export async function del(req, res, next) {
  let data;
  try {
    data = await eventBridge
      .putEvents({
        Entries: [
          {
            Source: "naska.ui",
            DetailType: "DELETE::SEN_NEED",
            Detail: JSON.stringify(req.body),
            EventBusName: "default",
            Time: new Date(),
          },
        ],
      })
      .promise();
  } catch (err) {
    res.end(JSON.stringify({ err }));
  }
  res.end(JSON.stringify({ data }));
}

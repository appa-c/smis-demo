import axios from "axios";
import { eventBridge } from "../../../_aws";

// Get all FSM Entries for a pupil
export async function get(req, res, next) {
  const { upn } = req.params;

  const url = `${process.env.AWS_API_GATEWAY_BASEURL}/pupils/${upn}/fsm/entries`;
  const data = await axios({
    url,
    method: "GET",
    headers: { "x-api-key": process.env.AWS_API_GATEWAY_KEY },
  });

  if (data !== null) {
    res.end(JSON.stringify(data.data));
  } else {
    next();
  }
}

// Create FSM Entry
export async function post(req, res, next) {
  let data;
  const fsmEntry = req.body;
  try {
    data = await eventBridge
      .putEvents({
        Entries: [
          {
            Source: "naska.ui",
            DetailType: "POST::FSM_ENTRY",
            Detail: JSON.stringify(fsmEntry),
            EventBusName: "default",
            Time: new Date(),
          },
        ],
      })
      .promise();
  } catch (err) {
    res.end(JSON.stringify({ err }));
  }
  res.end(JSON.stringify({ data }));
}

// Update FSM Entry
export async function put(req, res, next) {
  let data;
  const fsmEntry = req.body;
  try {
    data = await eventBridge
      .putEvents({
        Entries: [
          {
            Source: "naska.ui",
            DetailType: "PUT::FSM_ENTRY",
            Detail: JSON.stringify(fsmEntry),
            EventBusName: "default",
            Time: new Date(),
          },
        ],
      })
      .promise();
  } catch (err) {
    res.end(JSON.stringify({ err }));
  }
  res.end(JSON.stringify({ data }));
}

// Delete FSM Entry
export async function del(req, res, next) {
  let data;
  const fsmEntry = req.body;
  try {
    data = await eventBridge
      .putEvents({
        Entries: [
          {
            Source: "naska.ui",
            DetailType: "DELETE::FSM_ENTRY",
            Detail: JSON.stringify(fsmEntry),
            EventBusName: "default",
            Time: new Date(),
          },
        ],
      })
      .promise();
  } catch (err) {
    res.end(JSON.stringify({ err }));
  }
  res.end(JSON.stringify({ data }));
}

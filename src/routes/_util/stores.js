import { writable } from "svelte/store";

function createEventStore() {
  const { subscribe, set, update } = writable({
    sent: [],
    received: [],
  });

  return {
    subscribe,
    addSent: (newEvent) => {
      update((events) => {
        return {
          ...events,
          sent: [...events.sent, newEvent],
        };
      });
    },
    addReceived: (newEvent) => {
      update((events) => {
        return {
          ...events,
          received: [...events.received, newEvent],
        };
      });
    },
    clear: () => set({ sent: [], received: [] }),
  };
}

export const eventStore = createEventStore();

import axios from "axios";
import { eventBridge } from "../../_aws";

export async function get(req, res, next) {
  const url = `${process.env.AWS_API_GATEWAY_BASEURL}/pupils/${req.params.upn}/contacts`;
  const data = await axios({
    url,
    method: "GET",
    headers: { "x-api-key": process.env.AWS_API_GATEWAY_KEY },
  });

  if (data !== null) {
    res.end(JSON.stringify(data.data));
  } else {
    next();
  }
}

export async function post(req, res, next) {
  let data;
  const contact = req.body;
  try {
    data = await eventBridge
      .putEvents({
        Entries: [
          {
            Source: "naska.ui",
            Detail: JSON.stringify(contact),
            DetailType: "POST::CONTACT",
            EventBusName: "default",
            Time: new Date(),
          },
        ],
      })
      .promise();
  } catch (err) {
    console.log(err);
    res.end(JSON.stringify({ err }));
  }
  res.end(JSON.stringify({ data }));
}

export async function put(req, res, next) {
  let data;
  const contact = req.body;
  try {
    data = await eventBridge
      .putEvents({
        Entries: [
          {
            Source: "naska.ui",
            Detail: JSON.stringify(contact),
            DetailType: "PUT::CONTACT",
            EventBusName: "default",
            Time: new Date(),
          },
        ],
      })
      .promise();
  } catch (err) {
    console.log(err);
    res.end(JSON.stringify({ err }));
  }
  res.end(JSON.stringify({ data }));
}

export async function del(req, res, next) {
  let data;
  const contact = req.body;
  try {
    data = await eventBridge
      .putEvents({
        Entries: [
          {
            Source: "naska.ui",
            Detail: JSON.stringify(contact),
            DetailType: "DELETE::CONTACT",
            EventBusName: "default",
            Time: new Date(),
          },
        ],
      })
      .promise();
  } catch (err) {
    console.log(err);
    res.end(JSON.stringify({ err }));
  }
  res.end(JSON.stringify({ data }));
}

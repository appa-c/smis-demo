# SMIS Demo

## Getting Started

Run `npm install` to install dependencies, and `npm run dev` to run a local development version of the application.

## Environment Variables

AWS_ACCESS_KEY_ID
AWS_ACCESS_KEY_SECRET
AWS_API_GATEWAY_KEY
AWS_API_GATEWAY_BASEURL
AWS_SNS_TOPIC_ARN

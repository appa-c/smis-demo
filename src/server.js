require("dotenv").config();

import sirv from "sirv";
import polka from "polka";
import compression from "compression";
import * as sapper from "@sapper/server";
import { json, text } from "body-parser";
import socketio from "socket.io";

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === "development";

const { server } = polka() // You can also use Express
  .use(
    json(),
    text(),
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware()
  )
  .listen(PORT, (err) => {
    if (err) console.log("error", err);
  });

export const io = socketio(server);

import axios from "axios";
import AWS from "../../_aws";

const events = new AWS.EventBridge();

export async function get(req, res, next) {
  const url = `${process.env.AWS_API_GATEWAY_BASEURL}/pupils/${req.params.upn}`;
  const data = await axios({
    url,
    method: "GET",
    headers: { "x-api-key": process.env.AWS_API_GATEWAY_KEY },
  });

  if (data !== null) {
    res.end(JSON.stringify(data.data));
  } else {
    next();
  }
}

export async function put(req, res, next) {
  let data = {};
  try {
    data = await events
      .putEvents({
        Entries: [
          {
            Source: "naska.ui",
            Detail: JSON.stringify(req.body),
            DetailType: "PUT::PUPIL",
            EventBusName: "default",
            Time: new Date(),
          },
        ],
      })
      .promise();
  } catch (err) {
    console.log(err);
    res.end(
      JSON.stringify({
        err,
      })
    );
  }
  res.end(
    JSON.stringify({
      data,
    })
  );
}

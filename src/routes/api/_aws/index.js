import AWS from "aws-sdk";

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_ACCESS_KEY_SECRET,
  region: "eu-central-1",
  apiVersion: "latest",
});

export default AWS;

export const eventBridge = new AWS.EventBridge();

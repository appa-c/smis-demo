import axios from "axios";

export async function get(req, res, next) {
  const url = `${process.env.AWS_API_GATEWAY_BASEURL}/pupils/`;
  const data = await axios({
    url,
    method: "GET",
    headers: { "x-api-key": process.env.AWS_API_GATEWAY_KEY },
  });
  if (data !== null) {
    res.end(JSON.stringify(data.data));
  } else {
    next();
  }
}

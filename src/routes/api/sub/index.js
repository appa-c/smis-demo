import axios from "axios";
import { io } from "../../../server";

export async function post(req, res, next) {
  const reqBody = JSON.parse(req.body);

  if (reqBody.EventId) {
    // If the request is an SNS message with an EventId, emit the details to the socket
    const {
      EventId,
      EventResult,
      EventTimestamp,
      MessageTimestamp,
      Duration,
    } = JSON.parse(req.body);
    // console.log(EventId, EventResult);
    io.emit("event-response", {
      EventId,
      EventResult,
      state: EventResult.data ? "success" : "error",
      EventTimestamp,
      MessageTimestamp,
      Duration,
    });
  } else if (reqBody.SubscribeURL) {
    // else if the request body contains a subscribe url, assume it's a new subscription request from SNS
    const expectedTopicArn = process.env.AWS_SNS_TOPIC_ARN;
    if (reqBody.TopicArn === expectedTopicArn) {
      // verify that the subscription confirmation came from where we expect
      const subData = await axios(reqBody.SubscribeURL);
      console.log(JSON.parse(subData.data));
      res.end();
    }
  }

  res.end();
}

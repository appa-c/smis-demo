export function mapSenCode(code) {
  return {
    N: "No Special Educational Need",
    E: "Education, Health and Care Plan",
    K: "SEN Support",
  }[code];
}

export function mapSenTypeCode(code) {
  return {
    SPLD: "Specific Learning Difficulty",
    MLD: "Moderate Learning Difficult",
    SLD: "Severe Learning Difficulty",
    PMLD: "Profound &amp; Multiple Learning Difficulty",
    SEMH: "Social, Emotional and Mental Health",
    SLCN: "Speech, Language and Communication Needs",
    HI: "Hearing Impairment",
    VI: "Vision Impairment",
    MSI: "Multi-Sensory Impairment",
    PD: "Physical Disability",
    ASD: "Autistic Spectrum Disorder",
    OTH: "Other Difficulty/Disability",
    NSA: "No Specialist Assessment of Need",
  }[code];
}

export function sortAddressLines(addressLines) {
  const addressMap = {
    AddressLine1: "Address Line 1",
    AddressLine2: "Address Line 2",
    AddressLine3: "Address Line 3",
    AddressLine4: "Address Line 4",
    AddressLine5: "Address Line 5",
  };
  return Object.keys(addressLines)
    .sort()
    .map((line) => {
      return {
        friendly: addressMap[line],
        original: line,
      };
    });
}

export const senTypes = [
  { code: "SPLD", desc: "Specific Learning Difficulty" },
  { code: "MLD", desc: "Moderate Learning Difficult" },
  { code: "SLD", desc: "Severe Learning Difficulty" },
  { code: "PMLD", desc: "Profound &amp; Multiple Learning Difficulty" },
  { code: "SEMH", desc: "Social, Emotional and Mental Health" },
  { code: "SLCN", desc: "Speech, Language and Communication Needs" },
  { code: "HI", desc: "Hearing Impairment" },
  { code: "VI", desc: "Vision Impairment" },
  { code: "MSI", desc: "Multi-Sensory Impairment" },
  { code: "PD", desc: "Physical Disability" },
  { code: "ASD", desc: "Autistic Spectrum Disorder" },
  { code: "OTH", desc: "Other Difficulty/Disability" },
  { code: "NSA", desc: "No Specialist Assessment of Need" },
];

export const relationshipCodes = [
  { code: "CAR", desc: "Carer" },
  { code: "CAR", desc: "Carer" },
  { code: "CHM", desc: "Childminder" },
  { code: "DOC", desc: "Doctor" },
  { code: "FAM", desc: "Other Family Member" },
  { code: "FOF", desc: "Foster Father" },
  { code: "FOM", desc: "Foster Mother" },
  { code: "HTC", desc: "Head Teacher" },
  { code: "OTH", desc: "Other Contact" },
  { code: "PAF", desc: "Father" },
  { code: "PAM", desc: "Mother" },
  { code: "REL", desc: "Other Relative" },
  { code: "RLG", desc: "Religious/Spiritual Contact" },
  { code: "STF", desc: "Step Father" },
  { code: "STM", desc: "Step Mother" },
  { code: "SWR", desc: "Social Worker" },
  { code: "TCH", desc: "Teacher" },
];

export function mapRelationshipCode(code) {
  return relationshipCodes.reduce((map, relCode) => {
    return {
      ...map,
      [relCode.code]: `${relCode.desc} - (${relCode.code})`,
    };
  }, {})[code];
}

export const phoneCodes = [
  { code: "F", desc: "Fax" },
  { code: "H", desc: "Home" },
  { code: "A", desc: "Alternate Home" },
  { code: "M", desc: "Mobile" },
  { code: "W", desc: "Work" },
  { code: "D", desc: "Minicom" },
];

export function mapPhoneCode(code) {
  return phoneCodes.reduce((map, phoneCode) => {
    return {
      ...map,
      [phoneCode.code]: `${phoneCode.desc} - (${phoneCode.code})`,
    };
  }, {})[code];
}

// export function mapRelationshipCode(code) {
//   return {
//     CAR: "Carer - (CAR)",
//     CHM: "Childminder - (CHM)",
//     DOC: "Doctor - (DOC)",
//     FAM: "Other Family Member - (FAM)",
//     FOF: "Foster Father - (FOF)",
//     FOM: "Foster Mother - (FOM)",
//     HTC: "Head Teacher - (HTC)",
//     OTH: "Other Contact - (OTH)",
//     PAF: "Father - (PAF)",
//     PAM: "Mother - (PAM)",
//     REL: "Other Relative - (REL)",
//     RLG: "Religious/Spiritual Contact - (RLG)",
//     STF: "Step Father - (STF)",
//     STM: "Step Mother - (STM)",
//     SWR: "Social Worker - (SWR)",
//     TCH: "Teacher - (TCH)",
//   }[code];
// }

export function mapEntrolStatusCode(code) {
  return {
    C: "Current",
    M: "Main",
    S: "Subsidiary",
    F: "FE College",
    O: "Other",
    G: "Guest",
    P: "Previous",
  }[code];
}

export function mapServiceChildCode(code) {
  return {
    Y: "Yes",
    N: "No",
    U: "Unknown",
    R: "Refused",
  };
}

export const ethnicityCodes = {
  WBRI: "White - British",
  WCOR: "White - Cornish",
  WENG: "White - English",
  WSCO: "White - Scottish",
  WWEL: "White - Welsh",
  WOWB: "Other White British",
  WIRI: "White Irish",
  WIRT: "Traveller of Irish Heritage",
  WOTH: "Any Other White Background",
  WALB: "Albanian",
  WBOS: "Bosnian - Herzegovinian",
  WCRO: "Croation",
  WGRE: "Greek/Greek Cypriot",
  WGRK: "Greek",
  WGRC: "Greek Cypriot",
  WITA: "Italian",
  WKOS: "Kosovan",
  WPOR: "Protugese",
  WSER: "Serbian",
  WTUR: "Turkish/Turkish Cypriot",
  WTUK: "Turkish",
  WTUC: "Turkish Cypriot",
  WEUR: "White European",
  WEEU: "White Eastern European",
  WWEU: "White Western European",
  WOTW: "White Other",
  WROM: "Gypsy/Roma",
  WROG: "Gypsy",
  WROR: "Roma",
  WROO: "Other Gypsy/Roma",
  MWBC: "White and Black Caribbean",
  MWBA: "White and Black African",
  MWAS: "White and Asian",
  MWAP: "White and Pakistani",
  MWAI: "White and Indian",
  MWAO: "White and Any Other Asian Background",
  MOTH: "Any Other Mixed Background",
  MAOE: "Asian and Any Other Ethnic Group",
  MABL: "Asian and Black",
  MACH: "Asian and Chinese",
  MBOE: "Black and ANy Other Ethnic Group",
  MBCH: "Black and Chinese",
  MCOE: "Chinese and Any Other Ethnic Group",
  MWOE: "White and Any Other Ethnic Group",
  MWCH: "White and Chinese",
  MOTM: "Other Mixed Background",
  AIND: "Indian",
  APKN: "Pakistani",
  AMPK: "Mirpuri Pakistani",
  AKPA: "Kashmiri Pakistani",
  AOPK: "Other Pakistani",
  ABAN: "Bangladeshi",
  AOTH: "Any Other Asian Background",
  AAFR: "African Asian",
  AKAO: "Kashmiri Other",
  ANEP: "Nepali",
  ASNL: "Sri Lankan Sinhalese",
  ASLT: "Sri Lankan Tamil",
  ASRO: "Sri Lankan Other",
  AOTA: "Other Asian",
  BCRB: "Black Caribbean",
  BAFR: "Black - African",
  BANN: "Black - Angolan",
  BCON: "Black - Congolese",
  BGHA: "Black - Ghanaian",
  BNGN: "Black - Nigerian",
  BSLN: "Black - Sierra Leonean",
  BSOM: "Black - Somali",
  BSUD: "Black - Sudanese",
  BAOF: "Other Black African",
  BOTH: "Any Other Black Background",
  BEUR: "Black European",
  BNAM: "Black North American",
  BOTB: "Other Black",
  CHNE: "Chinese",
  CHKC: "Hong Kong Chinese",
  CMAL: "Malaysian Chinese",
  CSNG: "Singaporean Chinese",
  CTWN: "Taiwanese",
  COCH: "Other Chinese",
  OOTH: "Any Other Ethnic Group",
  OAFG: "Afghan",
  OARA: "Arab Other",
  OEGY: "Egyptian",
  OFIL: "Filipino",
  OIRN: "Iranian",
  OIRQ: "Iraqi",
  OJPN: "Japanese",
  OKOR: "Korean",
  OKRD: "Kurdish",
  OLAM: "Latin/South/Central American",
  OLEB: "Lebanese",
  OLIB: "Libyan",
  OMAL: "Malay",
  OMRC: "Moroccan",
  OPOL: "Polynesian",
  OTHA: "Thai",
  OVIE: "Vietnamese",
  OYEM: "Yemeni",
  OOEG: "Other Ethnic Group",
};

export function mapEthnicityCode(code) {
  return ethnicityCodes[code];
}
